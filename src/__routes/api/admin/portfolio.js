const express = require('express')
const router = express.Router()
const PortfolioController = require('../../../controllers/admin/PortfolioController')
const upload = require('../../../plugins/multer/portfolio')

router.route('/get-all')
    .get(PortfolioController.getAll)

router.route('/approved')
    .put(PortfolioController.approved)

router.route('/edit')
    .get(PortfolioController.edit)

router.route('/update')
    .put(upload.array('images', 5), PortfolioController.update)

router.route('/delete')
    .delete(PortfolioController.delete)

module.exports = router
