export const populateMessage = ['orderId', 'from', 'to', 
{ 
    path: 'quotationId',
    populate: {
        path: 'orderId',
        populate: ['freelancerId', 'employerId', 'portfolioId']
    }
}
]

export const populateOrder = ['employerId', 'freelancerId', 'portfolioId', 
{
    path: 'quotationId',
    populate: {
        path: 'orderId',
        populate: ['freelancerId', 'employerId', 'portfolioId']
    }
}
]