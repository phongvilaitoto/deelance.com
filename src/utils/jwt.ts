import Jwt from 'jsonwebtoken'

const secret: string = process.env.JWT_SECRET || 'phongvilaitoto'

export const signToken = (user: any) => {
    return Jwt.sign({   // Generate Token
        iss: secret,// secret code to generateToken
        sub: user._id,   // user.id or user._id
        iat: new Date().getTime(),  // Current time
        exp: new Date().setDate(new Date().getDate() + 365) // Current time + 1 Day
    }, secret) // secret key
}
