const multer = require('multer')

let makeid = length => {
    let result           = '';
    let characters       = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`;
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }


// Storage path & filename
const storage = multer.diskStorage({
    destination: (req, file, done) => {
        done(null, 'public/uploads/category')
    },
    filename: (req, file, done) => {
        const originalExtension = file.originalname.split('.').pop()
        done(null, makeid(14) + '.' + originalExtension)
    }
})

// Filter JPEG || PNG
const fileFilter = (req, file, done) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
        done(null, true)
    }
    else {
        done(null, false)
    }
}

// const upload & limit size
const upload = multer({
    storage,
    limit: {
        fileSize: 1024*1024 * 5
    },
    fileFilter
})

// Export upload
module.exports = upload
