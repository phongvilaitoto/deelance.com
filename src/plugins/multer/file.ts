import multer from 'multer'
import { v4 as uuidv4 } from 'uuid'

// Storage path & filename
const storage = multer.diskStorage({
    destination: 'public/deelance-static',
    filename: (_req, file, done) => {
        const originalExtension = file.originalname.split('.').pop()
        done(null, uuidv4() + '.' + originalExtension)
    },
    
})

// Filter JPEG || PNG
const fileFilter = (_req: any, file: any, done: any) => {
    if(file.mimetype === 'application/pdf' || file.mimetype === 'image/png' || file.mimetype === 'image/jpeg') {
        done(null, true)
    }
    else {
        done(null, false)
    }
}

// const upload & limit size
const upload = multer({
    storage,    
    fileFilter
})

// Export upload
export default upload
