import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import Mongo from './plugins/mongodb/mongodb' 
import dotenv from 'dotenv'
import expressHandlebars from 'express-handlebars'
import http from 'http'
import https from 'https'
import passport from './typescript/middlewares/passport'
import router from './typescript/apis/restful-api/routes'
import adminRouter from './typescript-admin/apis/routes'
import socketIo from 'socket.io'
import SocketIo from './typescript/socketio'
import fs from 'fs'


class App {
    static readonly PORT: number = 80
    #app!: express.Application | any
    #httpServer!: http.Server
    #httpsServer!: https.Server
    #mongo!: Mongo
    #cors!: any
    #io!: SocketIO.Server

    #jsonParser!: any
    #urlencodedParser!: any
    #dotenv!: any
    #port!: string | number
    #expressHandlebars!: any
    #corsOptions!: any

  

    constructor() {
       
        this.initialize()
        this.createApp()
        this.middleware()
        this.createRouter()
        this.createApollo()
        if(process.env.NODE_ENV === 'production') {
            this.createServerProduct()
        }else {
           this.createServer()
        }
        this.initSocket()
        this.listen()
    }


    private initialize(): void {
        this.#app = express()
        this.#cors = cors
        this.#mongo = new Mongo()

        this.#jsonParser = bodyParser.json()
        this.#urlencodedParser = bodyParser.urlencoded({ extended: false })
        this.#dotenv = dotenv
        this.#port = process.env.PORT || App.PORT
        this.#expressHandlebars = expressHandlebars
        this.#corsOptions = {
            origin: process.env.ORIGIN_URL,
            credentials: true
        }
    }

    private createApp(): void {
        this.#dotenv.config({ path: '.env' })
        this.#mongo.connect()
    }

    private middleware(): void {
        this.#app.use(this.#cors())
        this.#app.use(this.#jsonParser)
        this.#app.use(this.#urlencodedParser)

       // this.#app.use('/deelance-static', express.static('deelance-static', { dotfiles: 'allow' }))
        this.#app.use(express.static('public', { dotfiles: 'allow' }))
        this.#app.use(express.static('public/main', { dotfiles: 'allow' }))

        this.#app.engine('handlebars', this.#expressHandlebars())
        this.#app.set('view engine', 'handlebars')
      
        this.#app.use(passport.initialize())
        this.#app.use(passport.session())

    }

    private createApollo(): void {

    }

      // AWS
    //   private createServerAWS(): void {
    //     // Certificate
    //     const privateKey = fs.readFileSync('/etc/letsencrypt/live/mini.easyaccount.io/privkey.pem', 'utf8')
    //     const certificate = fs.readFileSync('/etc/letsencrypt/live/mini.easyaccount.io/cert.pem', 'utf8')
    //     const ca = fs.readFileSync('/etc/letsencrypt/live/mini.easyaccount.io/chain.pem', 'utf8')
    //     const credentials = {
    //         key: privateKey,
    //         cert: certificate,
    //         ca: ca
    //     }
    //     //none www
    //     this.#app.get('*', isAuth, (req: any, res: any) => {
    //         if (req.headers.host.match(/^www\./)) {
    //             res.redirect({ "Location": "https://" + req.headers['host'] + req.url }, 301 )
    //         }
    //         res.locals.meta = {
    //             title: 'easyaccount.io',
    //             description: 'Easy Account',
    //             keywords: 'easyaccount,easyaccount.io',
    //             ogTitle: 'EasyAccount.io',
    //           // ogImage: 'https://scontent.fvte4-1.fna.fbcdn.net/v/t1.0-9/71718690_1355150534636016_3760884131307716608_o.jpg?_nc_cat=107&_nc_sid=05277f&_nc_oc=AQmOdvpASZMis_Y768WARXsfiYhp9wTabdQjMPh4WH1h8VCJN81iNVtBuwkjrGNDrvg&_nc_ht=scontent.fvte4-1.fna&oh=fccbac997062acb38f878a63a8733133&oe=5E9E58A2',
    //             ogType: 'website',
    //             ogDescription: 'Easy Account'
    //         }
    //         res.render('index', { layout: false })
    //     })
    //     this.#httpServer = http.createServer((req: any, res: any) => {
    //         res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url })
    //         res.end()
    //     })
    //     this.#httpsServer = https.createServer(credentials, this.#app)
    // }

    private createServerProduct(): void {

        // if(process.env.NODE_ENV === 'production') {
        //     //Digital Ocean
        //     this.#httpsServer = https.createServer(
        //     {
        //         key: fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/privkey.pem'),
        //         cert: fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/cert.pem'),
        //         ca: fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/chain.pem')
        //     }),
        //     this.#app.listen(443, () => {
        //     console.log('Https Listening...')
        //     })

        //             this.#httpServer = http.createServer((req: any, res: any) => {
        //     res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url })
        //     res.end()
        // })
        // this.#httpsServer = https.createServer(credentials, this.#app)
        
            // this.#httpServer.createServer((req, res) => {
            //     res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
            //     res.end();
            // }).listen(80, () => {
            //     console.log('Http Listening...')
            // })
        //}

        const privateKey = fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/privkey.pem', 'utf8')
        const certificate = fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/cert.pem', 'utf8')
        const ca = fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/chain.pem', 'utf8')
        const credentials = {
            key: privateKey,
            cert: certificate,
            ca: ca
        }
        //none www
        this.#app.get('*', (req: any, res: any) => {
            if (req.headers.host.match(/^www\./)) {
                res.redirect({ "Location": "https://" + req.headers['host'] + req.url }, 301 )
            }
            res.locals.meta = {
                title: 'Deelance.com',
                description: 'Freelance website',
                keywords: 'deelance,deelance.com,freelance laos',
                ogTitle: 'Deelance.com',
               // ogImage: 'https://scontent.fvte4-1.fna.fbcdn.net/v/t1.0-9/71718690_1355150534636016_3760884131307716608_o.jpg?_nc_cat=107&_nc_sid=05277f&_nc_oc=AQmOdvpASZMis_Y768WARXsfiYhp9wTabdQjMPh4WH1h8VCJN81iNVtBuwkjrGNDrvg&_nc_ht=scontent.fvte4-1.fna&oh=fccbac997062acb38f878a63a8733133&oe=5E9E58A2',
                ogType: 'website',
                ogDescription: 'Deelance'
            }
            res.render('main', { layout: false })
        })
        this.#httpServer = http.createServer((req: any, res: any) => {
            res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url })
            res.end()
        })
        this.#httpsServer = https.createServer(credentials, this.#app)
    }

    // Localhost
    private createServer(): void {
       this.#app.get('*', (req: any, res: any) => {
            res.locals.meta = {
                title: 'Deelance.com',
                description: 'Freelance website',
                keywords: 'deelance,deelance.com,freelance laos',
                ogTitle: 'Deelance.com',
               // ogImage: 'https://scontent.fvte4-1.fna.fbcdn.net/v/t1.0-9/71718690_1355150534636016_3760884131307716608_o.jpg?_nc_cat=107&_nc_sid=05277f&_nc_oc=AQmOdvpASZMis_Y768WARXsfiYhp9wTabdQjMPh4WH1h8VCJN81iNVtBuwkjrGNDrvg&_nc_ht=scontent.fvte4-1.fna&oh=fccbac997062acb38f878a63a8733133&oe=5E9E58A2',
                ogType: 'website',
                ogDescription: 'Deelance'
            }
            res.render('main', { layout: false })
        })
        this.#httpServer = http.createServer(this.#app)
    }

    private createRouter(): void {
       this.#app.use('/api', router)
       this.#app.use('/admin/api', adminRouter)

    //    const passport = require('passport')
    //    const passportConf = require('./middleware/client/auth') // changed passport plugin
    //    const isAuth =  passport.authenticate('auth', { session: false }) // check Auth
    //    const isFreelancer =  passport.authenticate('freelancer', { session: false }) // check Auth
    //    const isEmployer =  passport.authenticate('employer', { session: false }) // check Auth
       
    //    //////////////////////////////// CLient Router //////////////////////////////////
       
    //    this.#app.use('/client/resume', require('./resume/rest-api/auth'))
    //    // no Middleware, can access all ( Page )
    //    this.#app.use('/client/home', require('./routes/api/client/home')) // Home router
    //    //app.use('/client/search', require('./routes/api/client/search')) // Search router
    //    this.#app.use('/client/auth', require('./routes/api/client/auth')) // auth router ( SignIn, SignUp )
    //    this.#app.use('/client/evidence', require('./routes/api/client/evidence')) // evidence router
       
    //    this.#app.use('/client/search', require('./routes/api/client/search')) // seacrh router
    //    this.#app.use('/client/category', require('./routes/api/client/category')) // category router
    //    this.#app.use('/client/review', require('./routes/api/client/review')) // ( isAuth inside) review router
       
    //    // Middleware isAuth
    //    this.#app.use('/client/profile', isAuth, require('./routes/api/client/profile')) // profile router
    //    this.#app.use('/client/message', isAuth, require('./routes/api/client/message')) // message router
    //    this.#app.use('/client/order', isAuth, require('./routes/api/client/order')) // ( isFreelancer inside ), order router
       
    //    // Only freelancer access
    //    this.#app.use('/client/portfolio',
       
    // //  [ isAuth, isFreelancer ],
       
    //    require('./routes/api/client/portfolio')) // portfolio router
       
    //    //////////////////////////////// Admin Router /////////////////////////////////////////
    //    this.#app.use('/admin/portfolio', require('./routes/api/admin/portfolio')) // admin portfolio
    //    this.#app.use('/admin/category', require('./routes/api/admin/category')) // admin category
    //    this.#app.use('/admin/user', require('./routes/api/admin/user')) // admin user
    //    this.#app.use('/admin/order', require('./routes/api/admin/order')) // admin order
    //    this.#app.use('/admin/evidence', require('./routes/api/admin/evidence')) // admin evidence

    }

    private initSocket(): void {
        this.#io = socketIo(this.#httpServer)
        this.#io.attach(3000, {
            pingInterval: 10000,
            pingTimeout: 5000,
            cookie: false
        })
        new SocketIo(this.#io)
    }

    private listen(): void {
        this.#httpServer.listen(this.#port, () => {
            console.log('Http is runing at',`http://localhost`)
        })
      //  if AWS
       if(process.env.NODE_ENV === 'production') {
          this.#httpsServer.listen(443, () => {
                console.log('Https is runing')
          })
      }
    }
}


new App()
