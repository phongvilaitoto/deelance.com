import { Router } from 'express'
import QuotationController from '../../../controllers/QuotationController'

const router: Router = Router()

router.route('/create-quotation')
    .post(new QuotationController().createQuotation)

export default router