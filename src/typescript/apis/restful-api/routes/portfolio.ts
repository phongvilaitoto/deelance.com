import { Router } from 'express'
import PortfolioController from '../../../controllers/PortfolioController'
import upload from '../../../../plugins/multer/file'
import { authenticate } from 'passport'
const isAuth = authenticate('isAuth', { session: false })

const router: Router = Router()

router.route('/get-portfolios')
    .get(new PortfolioController().portfolios)

router.route('/me/portfolio')
    .get(isAuth, new PortfolioController().mePortfolio)

router.route('/portfolio/:portfolioId')
    .get(new PortfolioController().portfolio)

router.route('/create-portfolio')
    .post(isAuth, upload.array('imageFiles', 30), new PortfolioController().createPortfolio)

router.route('/update-portfolio/:portfolioId')
    .put(isAuth, upload.array('imageFiles', 30), new PortfolioController().updatePortfolio)

router.route('/delete-portfolio/:portfolioId')
    .delete(isAuth, new PortfolioController().deletePortfolio)

export default router