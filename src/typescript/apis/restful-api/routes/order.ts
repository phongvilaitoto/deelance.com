import { Router } from 'express'
import OrderController from '../../../controllers/OrderController'
import upload from '../../../../plugins/multer/file'
import { authenticate } from 'passport'

const router: Router = Router()
const isAuth = authenticate('isAuth', { session: false })

router.route('/orders')
    .get(isAuth, new OrderController().orders)

router.route('/order/:id')
    .get(isAuth, new OrderController().order)

router.route('/create-order')
    .post(isAuth,new OrderController().createOrder)

router.route('/lalisa-order')
    .put(isAuth, new OrderController().lalisaOrder)    

router.route('/delivery-work')
    .put(isAuth, upload.array('files', 10), new OrderController().deliveryWork)

router.route('/review-work')
    .put(isAuth, new OrderController().reviewWork)

export default router