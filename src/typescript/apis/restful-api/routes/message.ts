import { Router } from 'express'
import MessageController from '../../../controllers/MessageController'
import upload from '../../../../plugins/multer/message'
import { authenticate } from 'passport'

const router: Router = Router()
const isAuth = authenticate('isAuth', { session: false })

router.route('/messages')
    .get(isAuth, new MessageController().messages)

router.route('/send-message')
    .post(isAuth, upload.single('image'), new MessageController().sendMessage)

export default router