import { Router } from 'express'
import AuthController from '../../../controllers/AuthController'
import { authenticate } from 'passport'
import upload from '../../../../plugins/multer/file'

const passportLocal = authenticate('local', { session: false })
const isAuth = authenticate('isAuth', { session: false })

const router: Router = Router()

router.route('/signup')
    .post(new AuthController().signUp)

router.route('/signin')
    .post(passportLocal, new AuthController().signIn)

router.route('/verify-email/:email/:verifyCode')
    .put(new AuthController().verifyEmail)

router.route('/verify-email-resend/:email')
    .put(new AuthController().verifyEmailResend)

router.route('/update-profile')
    .put(isAuth, upload.single('imageFile'), new AuthController().updateProfile)

router.route('/current-user')
    .get(isAuth, new AuthController().currentUser)

router.route('/apply-freelancer')
    .post(isAuth, upload.fields([
        { name: 'idCardImage', maxCount: 1 },
        { name: 'idCardImageWithFace', maxCount: 1 },
        { name: 'accountNumberImage', maxCount: 1 }
    ]), new AuthController().applyFreelancer)

export default router
