import auth from './auth'
import order from './order'
import message from './message'
import quotation from './quotation'
import portfolio from './portfolio'
import category from './category'


export default [ auth, order, message, quotation, portfolio, category ]