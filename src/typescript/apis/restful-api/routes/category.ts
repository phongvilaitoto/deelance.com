import { Router } from 'express'
import CategoryController from '../../../controllers/CategoryController'

const router: Router = Router()


router.route('/categories')
    .get(new CategoryController().categories)

export default router