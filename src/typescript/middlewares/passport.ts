
import passport from 'passport'
import passportJWT from 'passport-jwt'
import { Strategy as LocalStrategy } from 'passport-local'
import User from '../../models/User'
import bcrypt from 'bcrypt'

const JwtStrategy = passportJWT.Strategy
const ExtractJwt = passportJWT.ExtractJwt

const secret: string = process.env.JWT_SECRET || 'phongvilaitoto'


// used to serialize the user for the session
passport.serializeUser((user, done) => {
    done(null, user)
  })
// used to deserialize the user
passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
      done(err, user)
    })
})

passport.use('isAuth', new JwtStrategy({ // Use Strategy to find token from a user ( check Auth, return user from db )
    jwtFromRequest:  ExtractJwt.fromHeader('authorization'),
    secretOrKey: secret
}, async (payload: any, done: any) => {
        try {
            const user = await User.findById(payload.sub)  // Find the user specified in token
            if(!user) {  // If user doesn't exists, handle it
                return done(null, false)
            }
            done(null, user)  // Otherwise, return the user
        } catch (err) {
            done(err, false)
        }
}))

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, async (email, password, done) => {
    try { 
      const user: any = await User.findOne({ email })
      if(!user) {
          return done(null, false, { message: 'Incorrect email' })
      }
      const passwordMatch = bcrypt.compareSync(password, user.local.password)
      if(!passwordMatch) {
        return done(null, false, { message: 'Incorrect password' })
      } 
      done(null, user)
    } catch (e) {
        done(e, false)
    }
}))


export default passport