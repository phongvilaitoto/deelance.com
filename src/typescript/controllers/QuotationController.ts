import { Request, Response } from 'express'
import Order from '../../models/Order'
import Quotation from '../../models/Quotation'
import Message from '../../models/Message'
import { populateOrder, populateMessage } from '../../utils/populate'

export default class QuotationController {
    async createQuotation(req: Request, res: Response) {
        try {
            const {  
                orderId,
                price,
                description,
                workSteps,
                workEdit,
                workEditRemark,
                workDay,
                deadline,
                receive,
                remark,
                aggree 
            } = req.body

            const order: any = await Order.findById(orderId)

            const quotation: any = new Quotation({
                price,
                description,
                workSteps,
                workEdit,
                workEditRemark,
                workDay,
                deadline,
                receive,
                remark,
                aggree
            })
            quotation.orderId = order
            await quotation.save()

            const orderM: any = await Order.findById(orderId)
            orderM.quotationId = quotation
            await orderM.save()

            const message: any = new Message({
                from: order.freelancerId,
                to: order.employerId,
                type: 'quotation',
            })
            message.orderId = order
            message.quotationId = quotation
            await message.save()

            const orderFind = await Order.findById(orderId).populate(populateOrder)
            const messageFind = await Message.findById(message._id).populate(populateMessage)

            res.status(201).json({ order: orderFind, message: messageFind })
        } catch(e) {
            throw new Error(e)
        }
    }
}