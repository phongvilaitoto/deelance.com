import { Request, Response } from 'express'
import Category from '../../models/Category'

export default class CategoryController {
    async categories(req: Request, res: Response) {
        const categories = await Category.find().sort('sortOrder')
        res.status(200).json({ categories })
    }
}