
import { Request, Response } from 'express'
import Message from '../../models/Message'
import Order from '../../models/Order'
import User from '../../models/User'

import gmImageMagick from 'gm'
const gm = gmImageMagick.subClass({ imageMagick: true })

export default class MessageController {
    async messages(req: Request, res: Response) {
        const auth: any = req.user
        const { orderId } = req.query
        try {
            await Message.updateMany({
                $and: [
                    {
                        orderId, isRead: false
                    },
                    {
                        from: { $ne: auth._id }
                    }
                ]
            },{
                $set: { isRead: true }
            }, { runValidators: true, new: true })

            const messages = await Message.find({ orderId })
                .populate(['orderId', 'from', 'to', 
                { 
                    path: 'quotationId',
                    populate: {
                        path: 'orderId',
                        populate: ['freelancerId', 'employerId', 'portfolioId']
                    }
                }
            ])

            res.status(200).json({ messages })
        } catch (e) {
            throw new Error(e)
        }
    }

    async sendMessage (req: any, res: Response) {
        try {
            const { message, orderId } = req.body

            const order: any = await Order.findById(orderId)
            const authId = req.user._id.toString()
            
            const from = authId
            const to = authId === order.employerId.toString() ? order.freelancerId.toString() : order.employerId.toString()

            const fromUser = await User.findById(from)
            const toUser = await User.findById(to)

            if (req.file) { // if has file image
                /// Reisize
                let width = 600
                let height = 600
                gm(req.file.path)
                    .resize(width, height)
                    .gravity('Center')
                    .noProfile()
                    .write(req.file.path ,(err: any) => { // write a new file
                        if(err) return
                    })
                // Update Profile
                const image = 'uploads/message/' + req.file.filename // save new path to mongodb
                // new message

                const messageModel: any = new Message({
                    message,
                    from: fromUser,
                    to: toUser,
                    image
                })
                const order = await Order.findById(orderId)
                messageModel.orderId = order
                await messageModel.save()
                res.status(201).json({ message: messageModel })
            } else {
                const messageModel: any = new Message({
                    message,
                    from: fromUser,
                    to: toUser
                })
                const order = await Order.findById(orderId)
                messageModel.orderId = order
                await messageModel.save()
                res.status(201).json({ message: messageModel })
            }
        }
        catch (e) {
            throw new Error(e)
        }
    }
}