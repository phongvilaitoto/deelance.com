import { Request, Response } from 'express'
import Portfolio from '../../models/Portfolio'
import User from '../../models/User'
import Category from '../../models/Category'
import fs from 'fs'

export default class PortfolioController {
    
    async portfolios(req: Request, res: Response) {
        const { page, perPage, categoryId }: any = req.query
        const PerPage = perPage ? parseInt(perPage) : 10
        const Page = page ? parseInt(page): 1
        try {
            const portCate = await Portfolio.find({
                $and: [
                    categoryId ? { categoryId } : {},
                    { isApproved: true,  isDeleted: false }
                ]
            }).map(i=>i.length)
            const length = Math.ceil(portCate / perPage)
            const portfolios = await Portfolio.find({ $and: 
                [
                   categoryId ? { categoryId } : {}, 
                    { isApproved: true,  isDeleted: false }
                ] 
            })
                .populate(['userId', 'categoryId', 'reviewIds'])
                .sort({ createdAt: -1 })
                .skip((Page * perPage) - PerPage)
                .limit(PerPage)
            res.status(200).json({ portfolios, length })
        } catch (e) {
            throw e
        }
    }

    async portfolio(req: Request, res: Response) {
        try {
            const { portfolioId } = req.params
            const portfolio = await Portfolio.findOne({
                _id: portfolioId,
                isApproved: true,
                isDeleted: false
            })
            .populate(['userId', 'categoryId', {
                path: 'reviewIds',
                populate: ['userId']
            }])
            res.status(200).json({ portfolio })
        } catch(e) {
            throw new Error(e)
        }
    }


    // Auth
    async mePortfolio(req: Request, res: Response) {
        const auth: any = req.user
        const portfolios = await Portfolio.find({  userId: auth._id, isDeleted: false })
            .populate(['userId', 'categoryId', 'reviewIds'])
            .sort('-createdAt')
        res.status(200).json({ portfolios })
    }

    async createPortfolio(req: Request, res: Response) {
        const auth: any = req.user
        const { title, detail, categoryId, workSteps } = req.body
        const files: any = req.files
        
        const images = files.map((file: any) => {
            return file.filename
        })

        try {
            const category = await Category.findById(categoryId)

            const user = await User.findById(auth._id)
            const portfolio: any = new Portfolio({
                title,
                detail,
                images,
                workSteps,
                userId: user,
                categoryId: category,
                isApproved: true,
                status: 'approved'
            })
            await portfolio.save()
            
            res.status(201).json({ portfolio })
        } catch(e) {
            throw new Error(e)
        }
    }

    async updatePortfolio(req: Request, res: Response) {
        const auth: any = req.user
        const { portfolioId } = req.params
        const { title, detail, categoryId, workSteps, imagesToRemove } = req.body
        const files: any = req.files
    
        if(imagesToRemove) {
                imagesToRemove.map(async (image: string) => {
                    const oldImage = 'public/deelance-static/' + image
                    if(fs.existsSync(oldImage)) fs.unlinkSync(oldImage) 
                    await Portfolio.findByIdAndUpdate(portfolioId, {
                        // $set: { images: { $pull: { imagesToRemove } } }
                    }, { runValidators: true, new: true })
                })

        }
        if(files) {
            const portfolio: any = await Portfolio.findById(portfolioId)
            const newImages = files.map((file: any) => {
                return file.filename
            })
            portfolio.images.push(...newImages)
            await portfolio.save()
        }
        

        const category = await Category.findById(categoryId)
        const portfolio = await Portfolio.findByIdAndUpdate(portfolioId, {
            $set: {
                title,
                detail,
                workSteps,
                categoryId: category
            }
        }, { runValidators: true, new: true }).populate(['userId', 'categoryId', 'reviewIds'])

        res.status(200).json({ portfolio })

    }

    async deletePortfolio(req: Request, res: Response) {
        const { portfolioId } = req.params
        await Portfolio.findByIdAndUpdate(portfolioId, {
            $set: { 
                isDeleted: true,
                isPublished: false
            }
        }, { runValidators: true })
        res.status(200).json({ message: 'deleted' })
    }
}