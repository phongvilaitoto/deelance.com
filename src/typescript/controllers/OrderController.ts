import { Request, Response } from 'express'
import Order from '../../models/Order'
import User from '../../models/User'
import Portfolio from '../../models/Portfolio'
import Message from '../../models/Message'
import Review from '../../models/Review'
import { genOrderCode } from '../../utils/generate'

export default class OrderController {
    async orders(req: Request, res: Response) {
        const auth: any = req.user
        try {
            let orders: any = await Order.find({
                $or: [ { employerId: auth._id }, { freelancerId: auth._id } ],
            })
                .populate(['employerId', 'freelancerId', 'portfolioId', 
                {
                    path: 'quotationId',
                    populate: {
                        path: 'orderId',
                        populate: ['freelancerId', 'employerId', 'portfolioId']
                    }
                }
            ])
                .sort('-createdAt')
            res.status(200).json({ orders })
        } catch(e) {
            throw new Error(e)
        }
    }

    async order(req: Request, res: Response) {
        try {
            const orderId = req.params.id
            const order = await Order.findById(orderId)
                .populate(populate)
            res.status(200).json({ order })
        } catch(e) {
            throw new Error(e)
        }
    }

    async createOrder(req: Request, res: Response) {
        const { portfolioId, message } = req.body
        try {
            const auth: any = req.user
            const employerId = auth._id
            const portfolio: any = await Portfolio.findById(portfolioId)
            const freelancerId = portfolio.userId

            if(employerId.toString() === freelancerId.toString()) return res.status(400).json({ message: 'You r bad' })

            const freelancer = await User.findById(freelancerId)
            const employer = await User.findById(employerId)
            
            let orderCode = genOrderCode(8)
            //check if existing in db
            const isExist = await Order.findOne({ orderCode })
            if(isExist) {
               orderCode = genOrderCode(8)
            }
            const order:any = new Order({
                orderCode,
                employerId: employer,
                freelancerId: freelancer,
                portfolioId: portfolio
            })

            await order.save()
            // create Message
            const orderModel = await Order.findById(order._id)
            const messageModel: any = new Message({
                message,
                from: employer,
                to: freelancer,
                orderId: orderModel
            })
            await messageModel.save()
            res.status(201).json({ orderId: order._id })
        } catch(e) {
            throw new Error(e)
        }
    }


    async lalisaOrder(req: Request, res: Response) {
        try {
            const { orderId, event } = req.body
    
            let order!: any
            switch(event) {
                case 'payment': {
                    order = await Order.findByIdAndUpdate(orderId, {
                        $set: { status: 1, isPaid: true }
                    }, { runValidators: true, new: true }).populate(populate)
                    break
                }
                case 'startWorking': {
                    order = await Order.findByIdAndUpdate(orderId, {
                        $set: { status: 2, startWorking: true }
                    }, { runValidators: true, new: true }).populate(populate)
                    break
                }
                case 'approve': {
                    order = await Order.findByIdAndUpdate(orderId, {
                        $set: { status: 3 }
                    }, { runValidators: true, new: true }).populate(populate)
                    break 
                }
            }
            res.status(200).json({ order })

        } catch(e) {
            throw new Error(e)
        }
    }

    async deliveryWork(req: Request, res: Response) {
        const { orderId, link } = req.body
        try {
            let setItem: any = {
                deliverables: {}
            } 
            if(link) {
                setItem.deliverables.type = 'link'
                setItem.deliverables.link = link
            }else if(req.files.length) {
                const mapfiles: any = req.files
                const files: string[] = mapfiles.map((i: any) => i.path)
                setItem.deliverables.type = 'files'
                setItem.deliverables.files = files
            }else {
                setItem.deliverables.type = 'empty'
            }
            setItem.isDelivered = true
            setItem.deliverables.deliveryTime = new Date()
    
            const order = await Order.findByIdAndUpdate(orderId, {
                $set: setItem
            }, { runValidators: true, new: true }).populate(populate)
            res.status(200).json({ order })
        } catch(e) {
            throw new Error(e)
        }
    }

    async reviewWork(req: Request, res: Response) {
        const auth: any = req.user
        const { orderId, rating, comment } = req.body

        try {
            const isExistReview = await Review.findOne({ orderId })
            if(isExistReview) return res.status(409).json({ message: 'You are really bad' })

            const order: any = await Order.findById(orderId)
            const user = await User.findById(auth._id)
            const portfolio: any = await Portfolio.findById(order.portfolioId)

            const review: any = new Review({
                rating,
                comment
            })
            review.orderId = order
            review.userId = user
            review.portfolioId = portfolio
            await review.save()

            order.reviewId = review
            portfolio.reviewIds.push(review)
            await order.save()
            await portfolio.save()

            const orderM = await Order.findByIdAndUpdate(orderId, {
                $set: { status: 4 }
            }, { runValidators: true, new: true }).populate(populate)

            res.status(201).json({ order: orderM })
        } catch(e) {
            throw new Error(e)
        }

    }
}


const populate = ['employerId', 'freelancerId', 'portfolioId', 'reviewId', 
            {
                    path: 'quotationId',
                    populate: {
                        path: 'orderId',
                        populate: ['freelancerId', 'employerId', 'portfolioId']
                    }
                }
            ]

