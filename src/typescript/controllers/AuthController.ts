import { Request, Response } from 'express'
import User from '../../models/User'
import bcrypt from 'bcrypt'
import { signToken } from '../../utils/jwt'
import transporter from '../../utils/nodemailer'
import { genVerifyCode } from '../../utils/generate'
import ApplyFreelancer from '../../models/ApplyFreelancer'
import fs from 'fs'
import Category from '../../models/Category'

export default class AuthController {
    async currentUser(req: Request, res: Response) {
        const currentUser = req.user
        res.status(200).json({ currentUser })
    }

    async updateProfile(req: Request, res: Response) {
        const auth: any = req.user
        const file = req.file
        const { name, fullName, mobile, birthday } = req.body

        // const isExistMobile = await User.findOne({ mobile })
        // if(isExistMobile) {
        //     res.status(403).json({ message: 'Mobile is taken' })
        // }

        const form: any = {
            name,
            fullName,
            // mobile,
            birthday
        }
        try {
            if(file) {
                const oldImage = 'public/deelance-static/' + auth.image
                if(fs.existsSync(oldImage)) fs.unlinkSync(oldImage)
                form.image = file.filename
            }
    
            const user = await User.findByIdAndUpdate(auth._id, {
                $set: form
            }, { runValidators: true, new: true })

            res.status(200).json({ user })

        } catch(e) {
            throw new Error(e)
        }
    }

    async signIn(req: Request, res: Response) {
       const user: any = req.user
       if(!user.isEmailVerified) return res.status(400).json({ message: 'Your email has not been verified' }) 

       const accessToken = signToken(user)
       res.status(200).json({ accessToken })
    }

    async signUp(req: Request, res: Response) {
        const { username, email, password, confirmPassword, name, lastName, mobile } = req.body
        try {
            const isUsernameExist = await User.findOne({ username })
            const isEmailExist = await User.findOne({ email })
            const isMobileExist = await User.findOne({ mobile })
            if(isUsernameExist) return res.status(403).json({ message: 'Username is already in used' })
            if(isEmailExist) return res.status(403).json({ message: 'Email is already in used' })
            if(isMobileExist) return res.status(403).json({ message: 'Mobile is already in used' })
            if(password !== confirmPassword) return res.status(403).json({ message: 'Password and confirm password not matching' })
            
            const salt = bcrypt.genSaltSync(10)
            const hashedPassword = bcrypt.hashSync(password, salt)
            const verifyCode = genVerifyCode()

            const user = new User({
                username: username ? username : makeId(8),
                email,
                local: {
                    password: hashedPassword
                },
                name,
                lastName,
                mobile,
                verifyCode
            })
            await user.save()
            
            transporter.sendMail({
                to: email,
                subject: 'Confirm your email',
                html: `Your verification code is <span style="color: #1F9BD8">${verifyCode}</span>`,
            })

            res.status(201).json({ message: 'signined' })
        } catch(e) {
            throw new Error(e)
        }
    }


    async verifyEmail(req: Request, res: Response) {
        const { email, verifyCode } = req.params
        const user: any = await User.findOne({ email, verifyCode })
        if(!user) return res.status(403).json({ message: 'Verify code was invalid' })
        user.isEmailVerified = true
        user.verifyCode = undefined
        await user.save()
        const accessToken = signToken(user)
        res.status(200).json({ accessToken, user })
    }

    async verifyEmailResend(req: Request, res: Response) {
        const { email } = req.params
        const user: any = await User.findOne({ email })
        if(!user) return res.status(403).json({ message: 'Email was invalid' })
        const verifyCode = genVerifyCode()
        user.verifyCode = verifyCode
        await user.save()
        transporter.sendMail({
            to: email,
            subject: 'Confirm your email',
            html: `Your verification code is <span style="color: #1F9BD8">${verifyCode}</span>`,
        })
        res.status(200).json({ message: 'Verify code has been sent' })
     }

     async applyFreelancer(req: Request, res: Response) {
         const auth: any = req.user
         const { categoryId, aboutMe, idCardNumber, idCardName, idCardLastName, idCardAddress, bank, accountNumber } = req.body
         const files: any = req.files

         const idCardImage: string = files.idCardImage[0].filename
         const idCardImageWithFace: string = files.idCardImageWithFace[0].filename
         const accountNumberImage: string = files.accountNumberImage[0].filename

         try {  
             const category = await Category.findById(categoryId)
            const applyFreelancer = new ApplyFreelancer({
                categoryId: category,
                idCardImage,
                idCardImageWithFace,
                accountNumberImage,
                aboutMe,
                idCardNumber,
                idCardName,
                idCardLastName,
                idCardAddress,
                bank,
                accountNumber,
                userId: auth
            })
            await applyFreelancer.save()
            const user: any = await User.findByIdAndUpdate(auth._id, {
                $set: { isAppliedFreelancer: true }
            }, {  runValidators: true, new: true })
            await user.save()

            res.status(201).json({ applyFreelancer, user })
         } catch(e) {
             throw new Error(e)
         }
     }
}

const makeId = (length: number) => {
    var result           = '';
    var characters       = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    var charactersLength = characters.length
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
 }