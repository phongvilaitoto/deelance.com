export default class SocketIo {
    #io: SocketIO.Server
    constructor(io: SocketIO.Server) {
       this.#io = io
       this.init()
    }

    init() {
        this.#io.on('connection', (socket: SocketIO.Socket) => {
            socket.on('join', room => {
                socket.join(room)
            })            
            socket.on('send-message', ({ room, message }) => {
                socket.broadcast.to(room).emit('send-message-client', message)
            })

            socket.on('lalisa', ({ room, type, data }) => {
                socket.broadcast.to(room).emit('lalisa-client', { type, data })
            })

            socket.on('apply-freelancer',  ({ room, data }) => {
                socket.broadcast.to(room).emit('apply-freelancer-client', data)
            })

            // socket.on('leave', room => {
            //     socket.leave(room)
            // })
        })
    }
} 