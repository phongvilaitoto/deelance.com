
const http = require('http') // use http
const https = require('https') // use https
const fs = require('fs') // require fs
const app = require('./app') // require ./app


app.get('/mini*', (req, res) => {
    // none www
    if (req.headers.host.match(/^www\./)) {
         res.redirect({ "Location": "https://" + req.headers['host'] + req.url }, 301 )
    }
    return res.render('mini', { layout: false })
})

// app.get('*', (req, res) => {
//     return res.redirect('/mini')
// })
app.get('*', (req, res) => {
    return res.render('index', { layout: false })
})



// render index Vue js
//app.get('/*', (req, res) => {
    // none www
    //    if (req.headers.host.match(/^www\./)) {
    //      res.redirect({ "Location": "https://" + req.headers['host'] + req.url }, 301 )
    // }
    //return res.render('index', { layout: false })
//})


//Localhost


let server
if(process.env.NODE_ENV === 'production') {
    //Digital Ocean
    server = https.createServer(
    {
        key: fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/cert.pem'),
        ca: fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/chain.pem')
    },
    app).listen(443, () => {
    console.log('Https Listening...')
    })

    http.createServer((req, res) => {
        res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
        res.end();
    }).listen(80, () => {
        console.log('Http Listening...')
    })
}else {
    http.createServer(app).listen(80, () => {
        console.log('Localhost...')
    })    
}

const io = require('socket.io')(server) // create socketio server
require('./plugins/socketio/message')(io) // message socketio
require('./plugins/socketio/home')(io)
