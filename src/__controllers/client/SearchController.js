const Portfolio = require('../../models/Portfolio')

module.exports = {
   search: async (req, res, next) => {
       try {
           const { search, categoryId, rating = 'all' } = req.query
           const ports = await Portfolio.find({
               $and: [
                   { title : { $regex : search, $options : 'i' } },
                   { status: 'approved' },
                   categoryId ? { categoryId } : { }
               ]
           }).populate(['reviews', 'categoryId', 'author'])
           ports.forEach((portfolio) => {  // avarage rating
               const length = portfolio.reviews.length
               const arr = portfolio.reviews.map(i=>i.rating)
               const sum = arr.reduce((a, b) => a + b, 0)
               portfolio.rateAvg = sum/length
           })
           const portfolios = ports.filter((port) => {  // filter only rating >= query.rating
            if(rating === 'all') {
                return port
            } else if (rating === 'null') {
                return port.reviews.length === 0
            }
            return port.rateAvg >= rating
        })
        .sort((a, b) => {
           if(a.reviews.length > 0) {
               return b.rateAvg - a.rateAvg
           } else {
               return b.createdAt
           }
        })
           res.status(200).json({ portfolios })
       }catch (e) {
           throw new Error(e)
       }
   }
}
