const Evidence = require('../../models/Evidence')

module.exports = {
    getAll: async (req, res, next) => {
        const proofs = await Evidence.find()
            .sort('-createdAt')
        res.status(200).json({ proofs })
    }
}
