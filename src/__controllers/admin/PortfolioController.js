const Portfolio = require('../../models/Portfolio')
const fs = require('fs')
const gm = require('gm').subClass({ imageMagick: true })

module.exports = {
    getAll: async (req, res, next) => {
        const portfolios = await Portfolio.find()
            .populate(['author', 'categoryId'])
            .sort('-createdAt')
        res.status(200).json({ portfolios })
    },

    approved: async (req, res, next) => {
        const { portId } = req.body
        await Portfolio.updateOne({ _id: portId }, {
            $set: {
                status: 'approved'
            }
        })
        res.status(200).json({ success: 'portfolio has been approved' })
    },


    edit: async (req, res, next) => {
      try {
          const { portId } = req.query
          const portfolio = await Portfolio.findOne({ _id: portId })
              .populate(['categoryId'])
          res.status(200).json({ portfolio })
      } catch (e) {
          throw new Error(e)
      }
    },

    update: async  (req, res, next) => {
        try {
            const { title, description, imgPath, images, categoryId } = req.body
            const { _id } = req.query
            const files = req.files.map(i =>  `uploads/portfolio/${ i.filename }` ) // maping files in array
            // Resize
            const filePath = req.files.map(i => i.path)
            let width = 900
            let height = 900
            filePath.forEach(file => {
                gm(file)
                    .resize(width, height)
                    .gravity('Center')
                    .noProfile()
                    .write(file ,(err) => { // write a new file
                        if(err) return
                    })
            })
            if(images) { // if has current images
                // concat current and new in array
                const currentImages = images.map(i=>i) // maping current images
                const concatImages = currentImages.concat(files) // concat array images
                await Portfolio.updateOne({ _id },
                    { $set: {
                            title,
                            description,
                            images: concatImages,
                            categoryId,
                        }
                    })
            }else if(!images) { // if current images is null
                await Portfolio.updateOne({ _id },
                    { $set: { title,
                            description,
                            images: files,
                            categoryId,
                        }
                    })
            }
            if(imgPath) { // if imgPath has splice
                const deeleteImage = imgPath.map(i=>`public/${i}`)
                await deeleteImage.forEach(image => { // forEach is loop for Array && for is loop for object
                    fs.exists(image, (exists) => { // if file exists in public
                        if(exists) {
                            fs.unlinkSync(image) // delete this file
                        }
                    })
                })
            }
            res.status(200).json({ success: 'post portfolio successfully' })
        } catch (e) {
            throw new Error(e)
        }
    },

    // Delete portfolio
    delete: async (req, res, next) => {
        try {
            const {portId } = req.query // const { _id } from request params
           const portfolio = await Portfolio.findOne({ _id: portId })// find this portfolio
            const images = portfolio.images.map(i => `public/${ i }`) // maping images in array
            if(portfolio.images) { // if images not null
                images.forEach(image => { // forEach is loop for Array && for is loop for object
                    fs.exists(image, (exists) => { // if file exists in public
                        if(exists) {
                            fs.unlinkSync(image) // delete this file
                        }
                    })
                })
            }
            await portfolio.remove() // delete portfolio
            res.status(200).json({ success: 'Deelete portfolio successfully' }) // return response
        } catch (e) {
            throw new Error(e)
        }
    }
}
