const express = require('express')
const router = express.Router()
const AuthController = require('../classes/auth')

const passport = require('../../plugins/passport/passport')
const passportJWT =   passport.authenticate('jwt', { session: false }) // check Auth

router.route('/current-user')
    .get(passportJWT, AuthController.currentuser)

router.route('/update-resume')
    .put(passportJWT, AuthController.updateResume)

module.exports = router