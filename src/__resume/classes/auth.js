const User = require('../../models/User')

module.exports = {
    currentuser: async (req, res, next) => {
        const userId = req.user._id
        const user = await User.findById(userId)
        return res.status(200).json({ user })
    },
    updateResume: async (req, res, next) => {
        const userId = req.user._id
        const form = req.body
        const user = await User.findByIdAndUpdate(userId, {
           $set: form
        }, { new: true, runValidators: true })
       return res.status(200).json({ user })
    }
}