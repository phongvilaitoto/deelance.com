import { Request, Response } from 'express'
import Order from '../../models/Order'
import { populateOrder  } from '../../utils/populate'

export default class OrderController {
    async orders(_req: Request, res: Response) {
       try {
            const orders = await Order.find()
                .populate([ 'employerId', 'freelancerId' ])
                .sort('-createdAt')
        res.status(200).json({ orders })
       } catch(e) {
           throw new Error(e)
       }
    }

    async confirmPayment(req: Request, res: Response) {
        const { orderId } = req.body
        try {
            const order: any = await Order.findById(orderId)
            if(!order.quotationId) return res.status(400).json({ message: 'WTF' })
            const orderU = await Order.findByIdAndUpdate(orderId, {
                $set: { status: 1, isPaid: true }
            }, { runValidators: true, new: true })
            .populate(populateOrder)
            res.status(200).json({ order: orderU })
        } catch(e) {
            throw new Error(e)
        }
    }
}