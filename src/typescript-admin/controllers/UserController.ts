import User from '../../models/User'
import ApplyFreelancer from '../../models/ApplyFreelancer'
import { Request, Response } from 'express'

export default class UserController {
    async users (_req: Request, res: Response) {
        try {
            const users = await User.find()
            .sort('-createdAt')
            res.status(200).json({ users })
        } catch(e) {
            throw new Error(e)
        }
    }

    async applyFreelancers(_req: Request, res: Response) {
       try {
        const applyFreelancers = await ApplyFreelancer.find().populate('userId').sort('-createdAt')
        res.status(200).json({ applyFreelancers })
       } catch(e) {
           throw new Error(e)
       }
    }

    async applyFreelancer(req: Request, res: Response) {
        const { applyId } = req.params
        try {
            const applyFreelancer: any = await ApplyFreelancer.findById(applyId)
            const user = await User.findByIdAndUpdate(applyFreelancer.userId, {
                $set: { type: 'freelancer' }
            }, { runValidators: true, new: true })
            res.status(200).json({ user })
        } catch(e) {
            throw new Error(e)
        }
    }

    // async freelanceRegister (req: Request, res: Response) {
    //     const { userId, type, acceptFreelancer } = req.body
    //     await User.updateOne({ _id: userId }, {
    //         $set: {
    //             type,
    //             acceptFreelancer
    //         }
    //     })
    //     res.status(200).json({ success: 'update successful' })
    // }
}
