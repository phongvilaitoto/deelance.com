import { Router } from 'express'
import OrderController from '../../controllers/OrderController'

const router: Router = Router()

router.route('/orders')
    .get(new OrderController().orders)

router.route('/confirm-payment')
    .put(new OrderController().confirmPayment)

export default router