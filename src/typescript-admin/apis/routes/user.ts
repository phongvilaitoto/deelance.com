import { Router } from 'express'
import UserController from '../../controllers/UserController'

const router: Router = Router()

router.route('/users')
    .get(new UserController().users)

router.route('/apply-freelancers')
    .get(new UserController().applyFreelancers)

router.route('/apply-freelancer/:applyId')
    .put(new UserController().applyFreelancer)

export default router