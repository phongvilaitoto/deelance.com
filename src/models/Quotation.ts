import { Schema, model } from 'mongoose'

const quotationSchema = new Schema({
    orderId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'orders'
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    workSteps: [
        {
            type: String,
            required: true
        }
    ],
    workEdit: {
        type: Number,
        required: true
    },
    workEditRemark: {
        type: String,
        required: true
    },
    workDay: {
        type: Number,
        required: true
    },
    deadline: {
        type: Date,
        required: true
    },
    receive: {
        type: String,
        required: true
    },
    remark: String,
    aggree: {
        type: Boolean,
        required: true
    }
}, { timestamps: true })

const Quotation = model('quotations', quotationSchema)

export default Quotation