import { Schema, model } from 'mongoose'

const applyFreelancerSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true,
        //unique: true
    },
    categoryId: {
        type: Schema.Types.ObjectId,
        ref: 'categories',
        required: true
    },
    idCardImage: {
        type: String,
        required: true
    },
    idCardImageWithFace: {
        type: String,
        required: true
    },
    idCardNumber: {
        type: Number,
        required: true
    },
    idCardName: {
        type: String,
        required: true
    },
    idCardLastName: {
        type: String,
        required: true
    },
    idCardAddress: {
        type: String,
        required: true
    },
    aboutMe: {
        type: String,
        required: true
    },
    bank: {
        type: String,
        required: true
    },
    accountNumber: {
        type: String,
        required: true
    },
    accountNumberImage: {
        type: String,
        required: true
    }
}, { timestamps: true })

const ApplyFreelancer = model('applyFreelancers', applyFreelancerSchema, 'applyFreelancers')

export default ApplyFreelancer