import mongoose from 'mongoose'
const Schema = mongoose.Schema


const orderSchema = new Schema({
    orderCode: {
        type: String,
        required: true,
        unique: true
    },
    employerId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    freelancerId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    portfolioId: {
        type: Schema.Types.ObjectId,
        ref: 'portfolios',
        required: true
    },
    quotationId: {
        type: Schema.Types.ObjectId,
        ref: 'quotations',
        default: undefined
    },
    reviewId: {
        type: Schema.Types.ObjectId,
        ref: 'reviews',
        default: undefined
    },
    deliverables: {
        type: {
            type: String,
            enum: ['empty', 'link', 'files'],
        },
        link: String,
        files: {
            type: [String],
            default: undefined
        },
        deliveryTime: {
            type: Date,
        }
    },
    /**
     * 0 = created order
     * 1 = paid
     * 2 = working
     * 3 = review
     * 4 = success
     */ 
    status: {
        type: Number,
        enum: [0, 1, 2, 3, 4], 
        default: 0
    },
    isPaid: {
        type: Boolean,
        default: false
    },
    startWorking: {
        type: Boolean,
        default: false
    },
    isDelivered: {
        type: Boolean,
        default: false
    },
    // Quotation

    // quotation: {
    //     orderId: {
    //         type: String,
    //         default: shortid.generate,
    //         unique: true
    //     },
    //     information: {
    //         type: String,
    //     },
    //     price: {
    //         type: Number,
    //     },
    //     dayWork: {
    //         type: Number,
    //     },
    //     receive: {
    //         type: String,
    //     },
    //     optional: {
    //         type: String
    //     },
    //     paymentStatus: {
    //         type: Boolean
    //     },
    //     // maybe
    //     workStatus: {
    //         type: String,
    //         enum: [ 'wait for payment' ,'in progress', 'pending', 'successful', 'unsuccessful'],
    //     },
    //     createdAt: {
    //         type: Date
    //     }
    //     // ee
    //     // complete: {
    //     //     type: Boolean,
    //     //     default: false
    //     // },
    // },
    // Generate Key Id
}, { timestamps: true  })

orderSchema.pre('remove', async function (next) {
    next()
})


const Order = mongoose.model('orders', orderSchema)

export default Order
