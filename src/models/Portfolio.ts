import mongoose from 'mongoose'
const Schema = mongoose.Schema

// const Order = require('./Order')
// const Review = require('./Review')
// const Message = require('./Message')

const fs = require('fs')

const portfolioSchema = new Schema({
    images: {
        type: [String],
        required: true
    },
    title: {
        type: String,
        required: true
    },
    detail: {
        type: String,
        required: true
    },
    workSteps: {
        type: [String],
    },
    status: {
        type: String,
        enum: ['pending', 'rejected', 'approved'],
        default: 'pending'
    },
    isApproved: {
        type: Boolean,
        required: true,
        default: false
    },
    // Relation
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    categoryId: {
        type: Schema.Types.ObjectId,
        ref: 'categories',
        //required: true
    },
    reviewIds: [{
        type: Schema.Types.ObjectId,
        ref: 'reviews'
    }],
    isPublished: {
        type: Boolean,
        required: true,
        default: true
    },
    isDeleted: {
        type: Boolean,
        required: true,
        default: false
    }
},
    { timestamps: true  })

// Search
// portfolioSchema.index({ title: 'text' });

// Remove clear ref
// portfolioSchema.pre('remove', async function (next) {
//     const messages = await Message.find({ portfolioId: this._id })
//     messages.forEach(message => {
//         if(message.image) {
//             const allMessageImage = 'public/' + message.image // directory older image
//             fs.exists(allMessageImage, (exists) => {   // if file exists in public
//                 if(exists) {
//                     fs.unlinkSync(allMessageImage)     // delete this file
//                 }
//             })
//         }
//     })
//     await Message.deleteMany({ portfolioId: this._id })
//     await Order.deleteMany({ portfolioId: this._id })
//     await Review.deleteMany({ portfolioId: this._id })
// })

const Portfolio = mongoose.model('portfolios', portfolioSchema)

export default Portfolio
