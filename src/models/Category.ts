import mongoose from 'mongoose'
const Schema = mongoose.Schema

const categorySchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    image: {
        type: String,
        unique: true
    },
    sortOrder: {
        type: Number,
        required: true,
        unique: true
    },
}, { timestamps: true  })

const Category = mongoose.model('categories', categorySchema)

export default Category
