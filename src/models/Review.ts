import mongoose from 'mongoose'
const Schema = mongoose.Schema

const reviewSchema = new Schema({
    rating: {
        type: Number,
        required: true
    },
    comment: {
        type: String,
        required: false
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    orderId: {
        type: Schema.Types.ObjectId,
        ref:' orders',
        required: true
    },
    portfolioId: {
        type: Schema.Types.ObjectId,
        ref: 'portfolios',
        required: true
    },
}, { timestamps: true })

const Review = mongoose.model('reviews', reviewSchema)

export default Review
