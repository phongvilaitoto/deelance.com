import mongoose from 'mongoose'
const Schema = mongoose.Schema
const evidenceSchema = new Schema({
    orderId: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    confirmed: {
        type: Boolean,
        default: false
    }
}, { timestamps: true })

const Evidence = mongoose.model('proofs', evidenceSchema)

export default Evidence
