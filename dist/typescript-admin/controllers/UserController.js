"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = __importDefault(require("../../models/User"));
const ApplyFreelancer_1 = __importDefault(require("../../models/ApplyFreelancer"));
class UserController {
    async users(_req, res) {
        try {
            const users = await User_1.default.find()
                .sort('-createdAt');
            res.status(200).json({ users });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async applyFreelancers(_req, res) {
        try {
            const applyFreelancers = await ApplyFreelancer_1.default.find().populate('userId').sort('-createdAt');
            res.status(200).json({ applyFreelancers });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async applyFreelancer(req, res) {
        const { applyId } = req.params;
        try {
            const applyFreelancer = await ApplyFreelancer_1.default.findById(applyId);
            const user = await User_1.default.findByIdAndUpdate(applyFreelancer.userId, {
                $set: { type: 'freelancer' }
            }, { runValidators: true, new: true });
            res.status(200).json({ user });
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
exports.default = UserController;
