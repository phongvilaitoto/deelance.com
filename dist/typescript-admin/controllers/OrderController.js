"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Order_1 = __importDefault(require("../../models/Order"));
const populate_1 = require("../../utils/populate");
class OrderController {
    async orders(_req, res) {
        try {
            const orders = await Order_1.default.find()
                .populate(['employerId', 'freelancerId'])
                .sort('-createdAt');
            res.status(200).json({ orders });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async confirmPayment(req, res) {
        const { orderId } = req.body;
        try {
            const order = await Order_1.default.findById(orderId);
            if (!order.quotationId)
                return res.status(400).json({ message: 'WTF' });
            const orderU = await Order_1.default.findByIdAndUpdate(orderId, {
                $set: { status: 1, isPaid: true }
            }, { runValidators: true, new: true })
                .populate(populate_1.populateOrder);
            res.status(200).json({ order: orderU });
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
exports.default = OrderController;
