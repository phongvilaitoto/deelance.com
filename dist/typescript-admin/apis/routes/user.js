"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const UserController_1 = __importDefault(require("../../controllers/UserController"));
const router = express_1.Router();
router.route('/users')
    .get(new UserController_1.default().users);
router.route('/apply-freelancers')
    .get(new UserController_1.default().applyFreelancers);
router.route('/apply-freelancer/:applyId')
    .put(new UserController_1.default().applyFreelancer);
exports.default = router;
