"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const OrderController_1 = __importDefault(require("../../controllers/OrderController"));
const router = express_1.Router();
router.route('/orders')
    .get(new OrderController_1.default().orders);
router.route('/confirm-payment')
    .put(new OrderController_1.default().confirmPayment);
exports.default = router;
