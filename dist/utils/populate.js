"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.populateOrder = exports.populateMessage = void 0;
exports.populateMessage = ['orderId', 'from', 'to',
    {
        path: 'quotationId',
        populate: {
            path: 'orderId',
            populate: ['freelancerId', 'employerId', 'portfolioId']
        }
    }
];
exports.populateOrder = ['employerId', 'freelancerId', 'portfolioId',
    {
        path: 'quotationId',
        populate: {
            path: 'orderId',
            populate: ['freelancerId', 'employerId', 'portfolioId']
        }
    }
];
