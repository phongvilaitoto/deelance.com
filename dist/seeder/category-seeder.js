"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const categories = [
    {
        name: 'ອອກແບບກຣາຟຣິກ',
        sortOrder: 0
    },
    {
        name: 'ພັດທະນາເວັບໄຊທ໌',
        sortOrder: 1
    },
    {
        name: 'ຂຽນໂປຣແກຣມ ແລະ ພັດທະນາລະບົບ',
        sortOrder: 2
    },
    {
        name: 'ສະຖາປະນິກ ແລະ ວິສະວະກໍາ',
        sortOrder: 3
    },
    {
        name: 'ພັດທະນາແອັບ',
        sortOrder: 4
    },
    {
        name: 'ລະບົບເນັດເວີກ ແລະ ຖານຂໍ້ມູນ',
        sortOrder: 5
    },
    {
        name: 'ງານບໍລິການ',
        sortOrder: 6
    },
    {
        name: 'ຈັດງານ Event',
        sortOrder: 7
    },
    {
        name: 'ສອນພິເສດ',
        sortOrder: 8
    },
    {
        name: 'ບັນຊີ ແລະ ກົດໝາຍ',
        sortOrder: 9
    },
    {
        name: 'ອື່ນໆ',
        sortOrder: 10
    }
];
const Category_1 = __importDefault(require("../models/Category"));
class AddCate {
    constructor() {
    }
    async init() {
        Promise.all(categories.map(async (i) => {
            const cate = new Category_1.default({
                name: i.name,
                sortOrder: i.sortOrder
            });
            await cate.save();
        }));
    }
}
exports.default = AddCate;
