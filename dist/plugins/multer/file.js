"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const multer_1 = __importDefault(require("multer"));
const uuid_1 = require("uuid");
// Storage path & filename
const storage = multer_1.default.diskStorage({
    destination: 'public/deelance-static',
    filename: (_req, file, done) => {
        const originalExtension = file.originalname.split('.').pop();
        done(null, uuid_1.v4() + '.' + originalExtension);
    },
});
// Filter JPEG || PNG
const fileFilter = (_req, file, done) => {
    if (file.mimetype === 'application/pdf' || file.mimetype === 'image/png' || file.mimetype === 'image/jpeg') {
        done(null, true);
    }
    else {
        done(null, false);
    }
};
// const upload & limit size
const upload = multer_1.default({
    storage,
    fileFilter
});
// Export upload
exports.default = upload;
