"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const dbName = process.env.DB_NAME || 'deelance';
class Connect {
    constructor() {
        this.uri = `mongodb://127.0.0.1:27017/${dbName}`;
    }
    async connect() {
        try {
            await mongoose_1.default.connect(this.uri, {
                useNewUrlParser: true,
                useCreateIndex: true,
                useUnifiedTopology: true,
                useFindAndModify: false
            });
            console.log('Connected to', this.uri);
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
exports.default = Connect;
