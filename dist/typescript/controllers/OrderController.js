"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Order_1 = __importDefault(require("../../models/Order"));
const User_1 = __importDefault(require("../../models/User"));
const Portfolio_1 = __importDefault(require("../../models/Portfolio"));
const Message_1 = __importDefault(require("../../models/Message"));
const Review_1 = __importDefault(require("../../models/Review"));
const generate_1 = require("../../utils/generate");
class OrderController {
    async orders(req, res) {
        const auth = req.user;
        try {
            let orders = await Order_1.default.find({
                $or: [{ employerId: auth._id }, { freelancerId: auth._id }],
            })
                .populate(['employerId', 'freelancerId', 'portfolioId',
                {
                    path: 'quotationId',
                    populate: {
                        path: 'orderId',
                        populate: ['freelancerId', 'employerId', 'portfolioId']
                    }
                }
            ])
                .sort('-createdAt');
            res.status(200).json({ orders });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async order(req, res) {
        try {
            const orderId = req.params.id;
            const order = await Order_1.default.findById(orderId)
                .populate(populate);
            res.status(200).json({ order });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async createOrder(req, res) {
        const { portfolioId, message } = req.body;
        try {
            const auth = req.user;
            const employerId = auth._id;
            const portfolio = await Portfolio_1.default.findById(portfolioId);
            const freelancerId = portfolio.userId;
            if (employerId.toString() === freelancerId.toString())
                return res.status(400).json({ message: 'You r bad' });
            const freelancer = await User_1.default.findById(freelancerId);
            const employer = await User_1.default.findById(employerId);
            let orderCode = generate_1.genOrderCode(8);
            //check if existing in db
            const isExist = await Order_1.default.findOne({ orderCode });
            if (isExist) {
                orderCode = generate_1.genOrderCode(8);
            }
            const order = new Order_1.default({
                orderCode,
                employerId: employer,
                freelancerId: freelancer,
                portfolioId: portfolio
            });
            await order.save();
            // create Message
            const orderModel = await Order_1.default.findById(order._id);
            const messageModel = new Message_1.default({
                message,
                from: employer,
                to: freelancer,
                orderId: orderModel
            });
            await messageModel.save();
            res.status(201).json({ orderId: order._id });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async lalisaOrder(req, res) {
        try {
            const { orderId, event } = req.body;
            let order;
            switch (event) {
                case 'payment': {
                    order = await Order_1.default.findByIdAndUpdate(orderId, {
                        $set: { status: 1, isPaid: true }
                    }, { runValidators: true, new: true }).populate(populate);
                    break;
                }
                case 'startWorking': {
                    order = await Order_1.default.findByIdAndUpdate(orderId, {
                        $set: { status: 2, startWorking: true }
                    }, { runValidators: true, new: true }).populate(populate);
                    break;
                }
                case 'approve': {
                    order = await Order_1.default.findByIdAndUpdate(orderId, {
                        $set: { status: 3 }
                    }, { runValidators: true, new: true }).populate(populate);
                    break;
                }
            }
            res.status(200).json({ order });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async deliveryWork(req, res) {
        const { orderId, link } = req.body;
        try {
            let setItem = {
                deliverables: {}
            };
            if (link) {
                setItem.deliverables.type = 'link';
                setItem.deliverables.link = link;
            }
            else if (req.files.length) {
                const mapfiles = req.files;
                const files = mapfiles.map((i) => i.path);
                setItem.deliverables.type = 'files';
                setItem.deliverables.files = files;
            }
            else {
                setItem.deliverables.type = 'empty';
            }
            setItem.isDelivered = true;
            setItem.deliverables.deliveryTime = new Date();
            const order = await Order_1.default.findByIdAndUpdate(orderId, {
                $set: setItem
            }, { runValidators: true, new: true }).populate(populate);
            res.status(200).json({ order });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async reviewWork(req, res) {
        const auth = req.user;
        const { orderId, rating, comment } = req.body;
        try {
            const isExistReview = await Review_1.default.findOne({ orderId });
            if (isExistReview)
                return res.status(409).json({ message: 'You are really bad' });
            const order = await Order_1.default.findById(orderId);
            const user = await User_1.default.findById(auth._id);
            const portfolio = await Portfolio_1.default.findById(order.portfolioId);
            const review = new Review_1.default({
                rating,
                comment
            });
            review.orderId = order;
            review.userId = user;
            review.portfolioId = portfolio;
            await review.save();
            order.reviewId = review;
            portfolio.reviewIds.push(review);
            await order.save();
            await portfolio.save();
            const orderM = await Order_1.default.findByIdAndUpdate(orderId, {
                $set: { status: 4 }
            }, { runValidators: true, new: true }).populate(populate);
            res.status(201).json({ order: orderM });
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
exports.default = OrderController;
const populate = ['employerId', 'freelancerId', 'portfolioId', 'reviewId',
    {
        path: 'quotationId',
        populate: {
            path: 'orderId',
            populate: ['freelancerId', 'employerId', 'portfolioId']
        }
    }
];
