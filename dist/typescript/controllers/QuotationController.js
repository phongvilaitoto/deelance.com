"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Order_1 = __importDefault(require("../../models/Order"));
const Quotation_1 = __importDefault(require("../../models/Quotation"));
const Message_1 = __importDefault(require("../../models/Message"));
const populate_1 = require("../../utils/populate");
class QuotationController {
    async createQuotation(req, res) {
        try {
            const { orderId, price, description, workSteps, workEdit, workEditRemark, workDay, deadline, receive, remark, aggree } = req.body;
            const order = await Order_1.default.findById(orderId);
            const quotation = new Quotation_1.default({
                price,
                description,
                workSteps,
                workEdit,
                workEditRemark,
                workDay,
                deadline,
                receive,
                remark,
                aggree
            });
            quotation.orderId = order;
            await quotation.save();
            const orderM = await Order_1.default.findById(orderId);
            orderM.quotationId = quotation;
            await orderM.save();
            const message = new Message_1.default({
                from: order.freelancerId,
                to: order.employerId,
                type: 'quotation',
            });
            message.orderId = order;
            message.quotationId = quotation;
            await message.save();
            const orderFind = await Order_1.default.findById(orderId).populate(populate_1.populateOrder);
            const messageFind = await Message_1.default.findById(message._id).populate(populate_1.populateMessage);
            res.status(201).json({ order: orderFind, message: messageFind });
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
exports.default = QuotationController;
