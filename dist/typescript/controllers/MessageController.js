"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Message_1 = __importDefault(require("../../models/Message"));
const Order_1 = __importDefault(require("../../models/Order"));
const User_1 = __importDefault(require("../../models/User"));
const gm_1 = __importDefault(require("gm"));
const gm = gm_1.default.subClass({ imageMagick: true });
class MessageController {
    async messages(req, res) {
        const auth = req.user;
        const { orderId } = req.query;
        try {
            await Message_1.default.updateMany({
                $and: [
                    {
                        orderId, isRead: false
                    },
                    {
                        from: { $ne: auth._id }
                    }
                ]
            }, {
                $set: { isRead: true }
            }, { runValidators: true, new: true });
            const messages = await Message_1.default.find({ orderId })
                .populate(['orderId', 'from', 'to',
                {
                    path: 'quotationId',
                    populate: {
                        path: 'orderId',
                        populate: ['freelancerId', 'employerId', 'portfolioId']
                    }
                }
            ]);
            res.status(200).json({ messages });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async sendMessage(req, res) {
        try {
            const { message, orderId } = req.body;
            const order = await Order_1.default.findById(orderId);
            const authId = req.user._id.toString();
            const from = authId;
            const to = authId === order.employerId.toString() ? order.freelancerId.toString() : order.employerId.toString();
            const fromUser = await User_1.default.findById(from);
            const toUser = await User_1.default.findById(to);
            if (req.file) { // if has file image
                /// Reisize
                let width = 600;
                let height = 600;
                gm(req.file.path)
                    .resize(width, height)
                    .gravity('Center')
                    .noProfile()
                    .write(req.file.path, (err) => {
                    if (err)
                        return;
                });
                // Update Profile
                const image = 'uploads/message/' + req.file.filename; // save new path to mongodb
                // new message
                const messageModel = new Message_1.default({
                    message,
                    from: fromUser,
                    to: toUser,
                    image
                });
                const order = await Order_1.default.findById(orderId);
                messageModel.orderId = order;
                await messageModel.save();
                res.status(201).json({ message: messageModel });
            }
            else {
                const messageModel = new Message_1.default({
                    message,
                    from: fromUser,
                    to: toUser
                });
                const order = await Order_1.default.findById(orderId);
                messageModel.orderId = order;
                await messageModel.save();
                res.status(201).json({ message: messageModel });
            }
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
exports.default = MessageController;
