"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = __importDefault(require("../../models/User"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const jwt_1 = require("../../utils/jwt");
const nodemailer_1 = __importDefault(require("../../utils/nodemailer"));
const generate_1 = require("../../utils/generate");
const ApplyFreelancer_1 = __importDefault(require("../../models/ApplyFreelancer"));
const fs_1 = __importDefault(require("fs"));
const Category_1 = __importDefault(require("../../models/Category"));
class AuthController {
    async currentUser(req, res) {
        const currentUser = req.user;
        res.status(200).json({ currentUser });
    }
    async updateProfile(req, res) {
        const auth = req.user;
        const file = req.file;
        const { name, fullName, mobile, birthday } = req.body;
        // const isExistMobile = await User.findOne({ mobile })
        // if(isExistMobile) {
        //     res.status(403).json({ message: 'Mobile is taken' })
        // }
        const form = {
            name,
            fullName,
            // mobile,
            birthday
        };
        try {
            if (file) {
                const oldImage = 'public/deelance-static/' + auth.image;
                if (fs_1.default.existsSync(oldImage))
                    fs_1.default.unlinkSync(oldImage);
                form.image = file.filename;
            }
            const user = await User_1.default.findByIdAndUpdate(auth._id, {
                $set: form
            }, { runValidators: true, new: true });
            res.status(200).json({ user });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async signIn(req, res) {
        const user = req.user;
        if (!user.isEmailVerified)
            return res.status(400).json({ message: 'Your email has not been verified' });
        const accessToken = jwt_1.signToken(user);
        res.status(200).json({ accessToken });
    }
    async signUp(req, res) {
        const { username, email, password, confirmPassword, name, lastName, mobile } = req.body;
        try {
            const isUsernameExist = await User_1.default.findOne({ username });
            const isEmailExist = await User_1.default.findOne({ email });
            const isMobileExist = await User_1.default.findOne({ mobile });
            if (isUsernameExist)
                return res.status(403).json({ message: 'Username is already in used' });
            if (isEmailExist)
                return res.status(403).json({ message: 'Email is already in used' });
            if (isMobileExist)
                return res.status(403).json({ message: 'Mobile is already in used' });
            if (password !== confirmPassword)
                return res.status(403).json({ message: 'Password and confirm password not matching' });
            const salt = bcrypt_1.default.genSaltSync(10);
            const hashedPassword = bcrypt_1.default.hashSync(password, salt);
            const verifyCode = generate_1.genVerifyCode();
            const user = new User_1.default({
                username: username ? username : makeId(8),
                email,
                local: {
                    password: hashedPassword
                },
                name,
                lastName,
                mobile,
                verifyCode
            });
            await user.save();
            nodemailer_1.default.sendMail({
                to: email,
                subject: 'Confirm your email',
                html: `Your verification code is <span style="color: #1F9BD8">${verifyCode}</span>`,
            });
            res.status(201).json({ message: 'signined' });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async verifyEmail(req, res) {
        const { email, verifyCode } = req.params;
        const user = await User_1.default.findOne({ email, verifyCode });
        if (!user)
            return res.status(403).json({ message: 'Verify code was invalid' });
        user.isEmailVerified = true;
        user.verifyCode = undefined;
        await user.save();
        const accessToken = jwt_1.signToken(user);
        res.status(200).json({ accessToken, user });
    }
    async verifyEmailResend(req, res) {
        const { email } = req.params;
        const user = await User_1.default.findOne({ email });
        if (!user)
            return res.status(403).json({ message: 'Email was invalid' });
        const verifyCode = generate_1.genVerifyCode();
        user.verifyCode = verifyCode;
        await user.save();
        nodemailer_1.default.sendMail({
            to: email,
            subject: 'Confirm your email',
            html: `Your verification code is <span style="color: #1F9BD8">${verifyCode}</span>`,
        });
        res.status(200).json({ message: 'Verify code has been sent' });
    }
    async applyFreelancer(req, res) {
        const auth = req.user;
        const { categoryId, aboutMe, idCardNumber, idCardName, idCardLastName, idCardAddress, bank, accountNumber } = req.body;
        const files = req.files;
        const idCardImage = files.idCardImage[0].filename;
        const idCardImageWithFace = files.idCardImageWithFace[0].filename;
        const accountNumberImage = files.accountNumberImage[0].filename;
        try {
            const category = await Category_1.default.findById(categoryId);
            const applyFreelancer = new ApplyFreelancer_1.default({
                categoryId: category,
                idCardImage,
                idCardImageWithFace,
                accountNumberImage,
                aboutMe,
                idCardNumber,
                idCardName,
                idCardLastName,
                idCardAddress,
                bank,
                accountNumber,
                userId: auth
            });
            await applyFreelancer.save();
            const user = await User_1.default.findByIdAndUpdate(auth._id, {
                $set: { isAppliedFreelancer: true }
            }, { runValidators: true, new: true });
            await user.save();
            res.status(201).json({ applyFreelancer, user });
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
exports.default = AuthController;
const makeId = (length) => {
    var result = '';
    var characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
};
