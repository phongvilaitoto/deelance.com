"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Category_1 = __importDefault(require("../../models/Category"));
class CategoryController {
    async categories(req, res) {
        const categories = await Category_1.default.find().sort('sortOrder');
        res.status(200).json({ categories });
    }
}
exports.default = CategoryController;
