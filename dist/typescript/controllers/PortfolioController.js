"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Portfolio_1 = __importDefault(require("../../models/Portfolio"));
const User_1 = __importDefault(require("../../models/User"));
const Category_1 = __importDefault(require("../../models/Category"));
const fs_1 = __importDefault(require("fs"));
class PortfolioController {
    async portfolios(req, res) {
        const { page, perPage, categoryId } = req.query;
        const PerPage = perPage ? parseInt(perPage) : 10;
        const Page = page ? parseInt(page) : 1;
        try {
            const portCate = await Portfolio_1.default.find({
                $and: [
                    categoryId ? { categoryId } : {},
                    { isApproved: true, isDeleted: false }
                ]
            }).map(i => i.length);
            const length = Math.ceil(portCate / perPage);
            const portfolios = await Portfolio_1.default.find({ $and: [
                    categoryId ? { categoryId } : {},
                    { isApproved: true, isDeleted: false }
                ]
            })
                .populate(['userId', 'categoryId', 'reviewIds'])
                .sort({ createdAt: -1 })
                .skip((Page * perPage) - PerPage)
                .limit(PerPage);
            res.status(200).json({ portfolios, length });
        }
        catch (e) {
            throw e;
        }
    }
    async portfolio(req, res) {
        try {
            const { portfolioId } = req.params;
            const portfolio = await Portfolio_1.default.findOne({
                _id: portfolioId,
                isApproved: true,
                isDeleted: false
            })
                .populate(['userId', 'categoryId', {
                    path: 'reviewIds',
                    populate: ['userId']
                }]);
            res.status(200).json({ portfolio });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    // Auth
    async mePortfolio(req, res) {
        const auth = req.user;
        const portfolios = await Portfolio_1.default.find({ userId: auth._id, isDeleted: false })
            .populate(['userId', 'categoryId', 'reviewIds'])
            .sort('-createdAt');
        res.status(200).json({ portfolios });
    }
    async createPortfolio(req, res) {
        const auth = req.user;
        const { title, detail, categoryId, workSteps } = req.body;
        const files = req.files;
        const images = files.map((file) => {
            return file.filename;
        });
        try {
            const category = await Category_1.default.findById(categoryId);
            const user = await User_1.default.findById(auth._id);
            const portfolio = new Portfolio_1.default({
                title,
                detail,
                images,
                workSteps,
                userId: user,
                categoryId: category,
                isApproved: true,
                status: 'approved'
            });
            await portfolio.save();
            res.status(201).json({ portfolio });
        }
        catch (e) {
            throw new Error(e);
        }
    }
    async updatePortfolio(req, res) {
        const auth = req.user;
        const { portfolioId } = req.params;
        const { title, detail, categoryId, workSteps, imagesToRemove } = req.body;
        const files = req.files;
        if (imagesToRemove) {
            imagesToRemove.map(async (image) => {
                const oldImage = 'public/deelance-static/' + image;
                if (fs_1.default.existsSync(oldImage))
                    fs_1.default.unlinkSync(oldImage);
                await Portfolio_1.default.findByIdAndUpdate(portfolioId, {
                // $set: { images: { $pull: { imagesToRemove } } }
                }, { runValidators: true, new: true });
            });
        }
        if (files) {
            const portfolio = await Portfolio_1.default.findById(portfolioId);
            const newImages = files.map((file) => {
                return file.filename;
            });
            portfolio.images.push(...newImages);
            await portfolio.save();
        }
        const category = await Category_1.default.findById(categoryId);
        const portfolio = await Portfolio_1.default.findByIdAndUpdate(portfolioId, {
            $set: {
                title,
                detail,
                workSteps,
                categoryId: category
            }
        }, { runValidators: true, new: true }).populate(['userId', 'categoryId', 'reviewIds']);
        res.status(200).json({ portfolio });
    }
    async deletePortfolio(req, res) {
        const { portfolioId } = req.params;
        await Portfolio_1.default.findByIdAndUpdate(portfolioId, {
            $set: {
                isDeleted: true,
                isPublished: false
            }
        }, { runValidators: true });
        res.status(200).json({ message: 'deleted' });
    }
}
exports.default = PortfolioController;
