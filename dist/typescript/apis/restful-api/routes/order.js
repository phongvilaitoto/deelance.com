"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const OrderController_1 = __importDefault(require("../../../controllers/OrderController"));
const file_1 = __importDefault(require("../../../../plugins/multer/file"));
const passport_1 = require("passport");
const router = express_1.Router();
const isAuth = passport_1.authenticate('isAuth', { session: false });
router.route('/orders')
    .get(isAuth, new OrderController_1.default().orders);
router.route('/order/:id')
    .get(isAuth, new OrderController_1.default().order);
router.route('/create-order')
    .post(isAuth, new OrderController_1.default().createOrder);
router.route('/lalisa-order')
    .put(isAuth, new OrderController_1.default().lalisaOrder);
router.route('/delivery-work')
    .put(isAuth, file_1.default.array('files', 10), new OrderController_1.default().deliveryWork);
router.route('/review-work')
    .put(isAuth, new OrderController_1.default().reviewWork);
exports.default = router;
