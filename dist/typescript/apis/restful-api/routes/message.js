"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const MessageController_1 = __importDefault(require("../../../controllers/MessageController"));
const message_1 = __importDefault(require("../../../../plugins/multer/message"));
const passport_1 = require("passport");
const router = express_1.Router();
const isAuth = passport_1.authenticate('isAuth', { session: false });
router.route('/messages')
    .get(isAuth, new MessageController_1.default().messages);
router.route('/send-message')
    .post(isAuth, message_1.default.single('image'), new MessageController_1.default().sendMessage);
exports.default = router;
