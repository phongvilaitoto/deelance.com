"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const auth_1 = __importDefault(require("./auth"));
const order_1 = __importDefault(require("./order"));
const message_1 = __importDefault(require("./message"));
const quotation_1 = __importDefault(require("./quotation"));
const portfolio_1 = __importDefault(require("./portfolio"));
const category_1 = __importDefault(require("./category"));
exports.default = [auth_1.default, order_1.default, message_1.default, quotation_1.default, portfolio_1.default, category_1.default];
