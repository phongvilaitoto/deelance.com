"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const PortfolioController_1 = __importDefault(require("../../../controllers/PortfolioController"));
const file_1 = __importDefault(require("../../../../plugins/multer/file"));
const passport_1 = require("passport");
const isAuth = passport_1.authenticate('isAuth', { session: false });
const router = express_1.Router();
router.route('/get-portfolios')
    .get(new PortfolioController_1.default().portfolios);
router.route('/me/portfolio')
    .get(isAuth, new PortfolioController_1.default().mePortfolio);
router.route('/portfolio/:portfolioId')
    .get(new PortfolioController_1.default().portfolio);
router.route('/create-portfolio')
    .post(isAuth, file_1.default.array('imageFiles', 30), new PortfolioController_1.default().createPortfolio);
router.route('/update-portfolio/:portfolioId')
    .put(isAuth, file_1.default.array('imageFiles', 30), new PortfolioController_1.default().updatePortfolio);
router.route('/delete-portfolio/:portfolioId')
    .delete(isAuth, new PortfolioController_1.default().deletePortfolio);
exports.default = router;
