"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const CategoryController_1 = __importDefault(require("../../../controllers/CategoryController"));
const router = express_1.Router();
router.route('/categories')
    .get(new CategoryController_1.default().categories);
exports.default = router;
