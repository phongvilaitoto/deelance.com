"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const QuotationController_1 = __importDefault(require("../../../controllers/QuotationController"));
const router = express_1.Router();
router.route('/create-quotation')
    .post(new QuotationController_1.default().createQuotation);
exports.default = router;
