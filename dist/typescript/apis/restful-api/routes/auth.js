"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const AuthController_1 = __importDefault(require("../../../controllers/AuthController"));
const passport_1 = require("passport");
const file_1 = __importDefault(require("../../../../plugins/multer/file"));
const passportLocal = passport_1.authenticate('local', { session: false });
const isAuth = passport_1.authenticate('isAuth', { session: false });
const router = express_1.Router();
router.route('/signup')
    .post(new AuthController_1.default().signUp);
router.route('/signin')
    .post(passportLocal, new AuthController_1.default().signIn);
router.route('/verify-email/:email/:verifyCode')
    .put(new AuthController_1.default().verifyEmail);
router.route('/verify-email-resend/:email')
    .put(new AuthController_1.default().verifyEmailResend);
router.route('/update-profile')
    .put(isAuth, file_1.default.single('imageFile'), new AuthController_1.default().updateProfile);
router.route('/current-user')
    .get(isAuth, new AuthController_1.default().currentUser);
router.route('/apply-freelancer')
    .post(isAuth, file_1.default.fields([
    { name: 'idCardImage', maxCount: 1 },
    { name: 'idCardImageWithFace', maxCount: 1 },
    { name: 'accountNumberImage', maxCount: 1 }
]), new AuthController_1.default().applyFreelancer);
exports.default = router;
