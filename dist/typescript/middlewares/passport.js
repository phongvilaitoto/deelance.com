"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = __importDefault(require("passport"));
const passport_jwt_1 = __importDefault(require("passport-jwt"));
const passport_local_1 = require("passport-local");
const User_1 = __importDefault(require("../../models/User"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const JwtStrategy = passport_jwt_1.default.Strategy;
const ExtractJwt = passport_jwt_1.default.ExtractJwt;
const secret = process.env.JWT_SECRET || 'phongvilaitoto';
// used to serialize the user for the session
passport_1.default.serializeUser((user, done) => {
    done(null, user);
});
// used to deserialize the user
passport_1.default.deserializeUser((id, done) => {
    User_1.default.findById(id, (err, user) => {
        done(err, user);
    });
});
passport_1.default.use('isAuth', new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: secret
}, async (payload, done) => {
    try {
        const user = await User_1.default.findById(payload.sub); // Find the user specified in token
        if (!user) { // If user doesn't exists, handle it
            return done(null, false);
        }
        done(null, user); // Otherwise, return the user
    }
    catch (err) {
        done(err, false);
    }
}));
passport_1.default.use(new passport_local_1.Strategy({
    usernameField: 'email',
    passwordField: 'password'
}, async (email, password, done) => {
    try {
        const user = await User_1.default.findOne({ email });
        if (!user) {
            return done(null, false, { message: 'Incorrect email' });
        }
        const passwordMatch = bcrypt_1.default.compareSync(password, user.local.password);
        if (!passwordMatch) {
            return done(null, false, { message: 'Incorrect password' });
        }
        done(null, user);
    }
    catch (e) {
        done(e, false);
    }
}));
exports.default = passport_1.default;
