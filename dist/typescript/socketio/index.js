"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SocketIo {
    constructor(io) {
        this.#io = io;
        this.init();
    }
    #io;
    init() {
        this.#io.on('connection', (socket) => {
            socket.on('join', room => {
                socket.join(room);
            });
            socket.on('send-message', ({ room, message }) => {
                socket.broadcast.to(room).emit('send-message-client', message);
            });
            socket.on('lalisa', ({ room, type, data }) => {
                socket.broadcast.to(room).emit('lalisa-client', { type, data });
            });
            socket.on('apply-freelancer', ({ room, data }) => {
                socket.broadcast.to(room).emit('apply-freelancer-client', data);
            });
            // socket.on('leave', room => {
            //     socket.leave(room)
            // })
        });
    }
}
exports.default = SocketIo;
