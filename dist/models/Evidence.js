"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
const evidenceSchema = new Schema({
    orderId: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    confirmed: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });
const Evidence = mongoose_1.default.model('proofs', evidenceSchema);
exports.default = Evidence;
