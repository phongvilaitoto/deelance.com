"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const quotationSchema = new mongoose_1.Schema({
    orderId: {
        type: mongoose_1.Schema.Types.ObjectId,
        required: true,
        ref: 'orders'
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    workSteps: [
        {
            type: String,
            required: true
        }
    ],
    workEdit: {
        type: Number,
        required: true
    },
    workEditRemark: {
        type: String,
        required: true
    },
    workDay: {
        type: Number,
        required: true
    },
    deadline: {
        type: Date,
        required: true
    },
    receive: {
        type: String,
        required: true
    },
    remark: String,
    aggree: {
        type: Boolean,
        required: true
    }
}, { timestamps: true });
const Quotation = mongoose_1.model('quotations', quotationSchema);
exports.default = Quotation;
