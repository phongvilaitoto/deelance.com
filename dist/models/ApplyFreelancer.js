"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const applyFreelancerSchema = new mongoose_1.Schema({
    userId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'users',
        required: true,
    },
    categoryId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'categories',
        required: true
    },
    idCardImage: {
        type: String,
        required: true
    },
    idCardImageWithFace: {
        type: String,
        required: true
    },
    idCardNumber: {
        type: Number,
        required: true
    },
    idCardName: {
        type: String,
        required: true
    },
    idCardLastName: {
        type: String,
        required: true
    },
    idCardAddress: {
        type: String,
        required: true
    },
    aboutMe: {
        type: String,
        required: true
    },
    bank: {
        type: String,
        required: true
    },
    accountNumber: {
        type: String,
        required: true
    },
    accountNumberImage: {
        type: String,
        required: true
    }
}, { timestamps: true });
const ApplyFreelancer = mongoose_1.model('applyFreelancers', applyFreelancerSchema, 'applyFreelancers');
exports.default = ApplyFreelancer;
