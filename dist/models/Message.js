"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
const messageSchema = new Schema({
    from: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    to: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    orderId: {
        type: Schema.Types.ObjectId,
        ref: 'orders',
        required: true,
    },
    // portfolioId: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'portfolios',
    //   required: true
    // },
    message: {
        type: String
    },
    isRead: {
        type: Boolean,
        required: true,
        default: false
    },
    image: {
        type: String
    },
    type: {
        type: String,
        enum: ['message', 'quotation'],
        required: true,
        default: 'message'
    },
    quotationId: {
        type: Schema.Types.ObjectId,
        ref: 'quotations'
    }
}, { timestamps: true });
const Message = mongoose_1.default.model('messages', messageSchema);
exports.default = Message;
