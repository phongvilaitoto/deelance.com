"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose")); // require mongoose from mongodb.js
const Schema = mongoose_1.default.Schema; // require Schema from mongodb.js
const userSchema = new Schema({
    method: {
        type: String,
        enum: ['local'],
        required: true,
        default: 'local'
    },
    type: {
        type: String,
        enum: ['employer', 'freelancer'],
        required: true,
        default: 'employer'
    },
    // schema all
    username: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    mobile: {
        type: Number,
        unique: true,
        required: true,
    },
    image: String,
    name: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    birthday: {
        type: Date
    },
    local: {
        password: {
            type: String,
            required: true
        }
    },
    verifyCode: {
        type: Number
    },
    // village: String,
    // district: String,
    // province: String,
    // skill: [String],
    // workHistory: [String],
    isEmailVerified: {
        type: Boolean,
        default: false,
        required: true
    },
    isMobileVerified: {
        type: Boolean,
        default: false,
        required: true
    },
    isAcceptOrder: Boolean,
    isAppliedFreelancer: Boolean,
    isFreelancerApproved: {
        type: String,
    },
}, { timestamps: true }); // version key to inject
// userSchema.pre('save', async function (next) { // Pre save() + hash
//      try {
//          if(this.method === 'google') {
//              next()
//          }
//          if(this.method === 'facebook') {
//              next()
//          }
//          const salt = await bcrypt.genSalt(10)  // Generate a salt
//          const passwordHash = await bcrypt.hash(this.local.password, salt)  // Generate a password hash ( salt, hash )
//          this.local.password = passwordHash  // Re-assign hashed version over original, plain text password
//          next()
//      } catch (err) {
//          throw new Error(err)
//      }
// })
// userSchema.methods.isValidPassword = async function (newPassword) { // Check isValidPassword
//     try {
//        return await bcrypt.compare(newPassword, this.local.password) // compare password & hash
//     } catch(err) {
//         throw new Error(err)
//     }
// }
userSchema.methods.toJSON = function () {
    let obj = this.toObject();
    delete obj.local;
    return obj;
};
const User = mongoose_1.default.model('users', userSchema); // Create a models
exports.default = User; // Export User Model
