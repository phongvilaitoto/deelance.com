"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const mongodb_1 = __importDefault(require("./plugins/mongodb/mongodb"));
const dotenv_1 = __importDefault(require("dotenv"));
const express_handlebars_1 = __importDefault(require("express-handlebars"));
const http_1 = __importDefault(require("http"));
const https_1 = __importDefault(require("https"));
const passport_1 = __importDefault(require("./typescript/middlewares/passport"));
const routes_1 = __importDefault(require("./typescript/apis/restful-api/routes"));
const routes_2 = __importDefault(require("./typescript-admin/apis/routes"));
const socket_io_1 = __importDefault(require("socket.io"));
const socketio_1 = __importDefault(require("./typescript/socketio"));
const fs_1 = __importDefault(require("fs"));
let App = /** @class */ (() => {
    class App {
        constructor() {
            this.initialize();
            this.createApp();
            this.middleware();
            this.createRouter();
            this.createApollo();
            if (process.env.NODE_ENV === 'production') {
                this.createServerProduct();
            }
            else {
                this.createServer();
            }
            this.initSocket();
            this.listen();
        }
        #app;
        #httpServer;
        #httpsServer;
        #mongo;
        #cors;
        #io;
        #jsonParser;
        #urlencodedParser;
        #dotenv;
        #port;
        #expressHandlebars;
        #corsOptions;
        initialize() {
            this.#app = express_1.default();
            this.#cors = cors_1.default;
            this.#mongo = new mongodb_1.default();
            this.#jsonParser = body_parser_1.default.json();
            this.#urlencodedParser = body_parser_1.default.urlencoded({ extended: false });
            this.#dotenv = dotenv_1.default;
            this.#port = process.env.PORT || App.PORT;
            this.#expressHandlebars = express_handlebars_1.default;
            this.#corsOptions = {
                origin: process.env.ORIGIN_URL,
                credentials: true
            };
        }
        createApp() {
            this.#dotenv.config({ path: '.env' });
            this.#mongo.connect();
        }
        middleware() {
            this.#app.use(this.#cors());
            this.#app.use(this.#jsonParser);
            this.#app.use(this.#urlencodedParser);
            // this.#app.use('/deelance-static', express.static('deelance-static', { dotfiles: 'allow' }))
            this.#app.use(express_1.default.static('public', { dotfiles: 'allow' }));
            this.#app.use(express_1.default.static('public/main', { dotfiles: 'allow' }));
            this.#app.engine('handlebars', this.#expressHandlebars());
            this.#app.set('view engine', 'handlebars');
            this.#app.use(passport_1.default.initialize());
            this.#app.use(passport_1.default.session());
        }
        createApollo() {
        }
        // AWS
        //   private createServerAWS(): void {
        //     // Certificate
        //     const privateKey = fs.readFileSync('/etc/letsencrypt/live/mini.easyaccount.io/privkey.pem', 'utf8')
        //     const certificate = fs.readFileSync('/etc/letsencrypt/live/mini.easyaccount.io/cert.pem', 'utf8')
        //     const ca = fs.readFileSync('/etc/letsencrypt/live/mini.easyaccount.io/chain.pem', 'utf8')
        //     const credentials = {
        //         key: privateKey,
        //         cert: certificate,
        //         ca: ca
        //     }
        //     //none www
        //     this.#app.get('*', isAuth, (req: any, res: any) => {
        //         if (req.headers.host.match(/^www\./)) {
        //             res.redirect({ "Location": "https://" + req.headers['host'] + req.url }, 301 )
        //         }
        //         res.locals.meta = {
        //             title: 'easyaccount.io',
        //             description: 'Easy Account',
        //             keywords: 'easyaccount,easyaccount.io',
        //             ogTitle: 'EasyAccount.io',
        //           // ogImage: 'https://scontent.fvte4-1.fna.fbcdn.net/v/t1.0-9/71718690_1355150534636016_3760884131307716608_o.jpg?_nc_cat=107&_nc_sid=05277f&_nc_oc=AQmOdvpASZMis_Y768WARXsfiYhp9wTabdQjMPh4WH1h8VCJN81iNVtBuwkjrGNDrvg&_nc_ht=scontent.fvte4-1.fna&oh=fccbac997062acb38f878a63a8733133&oe=5E9E58A2',
        //             ogType: 'website',
        //             ogDescription: 'Easy Account'
        //         }
        //         res.render('index', { layout: false })
        //     })
        //     this.#httpServer = http.createServer((req: any, res: any) => {
        //         res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url })
        //         res.end()
        //     })
        //     this.#httpsServer = https.createServer(credentials, this.#app)
        // }
        createServerProduct() {
            // if(process.env.NODE_ENV === 'production') {
            //     //Digital Ocean
            //     this.#httpsServer = https.createServer(
            //     {
            //         key: fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/privkey.pem'),
            //         cert: fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/cert.pem'),
            //         ca: fs.readFileSync('/etc/letsencrypt/live/deelance.com-0001/chain.pem')
            //     }),
            //     this.#app.listen(443, () => {
            //     console.log('Https Listening...')
            //     })
            //             this.#httpServer = http.createServer((req: any, res: any) => {
            //     res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url })
            //     res.end()
            // })
            // this.#httpsServer = https.createServer(credentials, this.#app)
            // this.#httpServer.createServer((req, res) => {
            //     res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
            //     res.end();
            // }).listen(80, () => {
            //     console.log('Http Listening...')
            // })
            //}
            const privateKey = fs_1.default.readFileSync('/etc/letsencrypt/live/deelance.com-0001/privkey.pem', 'utf8');
            const certificate = fs_1.default.readFileSync('/etc/letsencrypt/live/deelance.com-0001/cert.pem', 'utf8');
            const ca = fs_1.default.readFileSync('/etc/letsencrypt/live/deelance.com-0001/chain.pem', 'utf8');
            const credentials = {
                key: privateKey,
                cert: certificate,
                ca: ca
            };
            //none www
            this.#app.get('*', (req, res) => {
                if (req.headers.host.match(/^www\./)) {
                    res.redirect({ "Location": "https://" + req.headers['host'] + req.url }, 301);
                }
                res.locals.meta = {
                    title: 'Deelance.com',
                    description: 'Freelance website',
                    keywords: 'deelance,deelance.com,freelance laos',
                    ogTitle: 'Deelance.com',
                    // ogImage: 'https://scontent.fvte4-1.fna.fbcdn.net/v/t1.0-9/71718690_1355150534636016_3760884131307716608_o.jpg?_nc_cat=107&_nc_sid=05277f&_nc_oc=AQmOdvpASZMis_Y768WARXsfiYhp9wTabdQjMPh4WH1h8VCJN81iNVtBuwkjrGNDrvg&_nc_ht=scontent.fvte4-1.fna&oh=fccbac997062acb38f878a63a8733133&oe=5E9E58A2',
                    ogType: 'website',
                    ogDescription: 'Deelance'
                };
                res.render('main', { layout: false });
            });
            this.#httpServer = http_1.default.createServer((req, res) => {
                res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
                res.end();
            });
            this.#httpsServer = https_1.default.createServer(credentials, this.#app);
        }
        // Localhost
        createServer() {
            this.#app.get('*', (req, res) => {
                res.locals.meta = {
                    title: 'Deelance.com',
                    description: 'Freelance website',
                    keywords: 'deelance,deelance.com,freelance laos',
                    ogTitle: 'Deelance.com',
                    // ogImage: 'https://scontent.fvte4-1.fna.fbcdn.net/v/t1.0-9/71718690_1355150534636016_3760884131307716608_o.jpg?_nc_cat=107&_nc_sid=05277f&_nc_oc=AQmOdvpASZMis_Y768WARXsfiYhp9wTabdQjMPh4WH1h8VCJN81iNVtBuwkjrGNDrvg&_nc_ht=scontent.fvte4-1.fna&oh=fccbac997062acb38f878a63a8733133&oe=5E9E58A2',
                    ogType: 'website',
                    ogDescription: 'Deelance'
                };
                res.render('main', { layout: false });
            });
            this.#httpServer = http_1.default.createServer(this.#app);
        }
        createRouter() {
            this.#app.use('/api', routes_1.default);
            this.#app.use('/admin/api', routes_2.default);
            //    const passport = require('passport')
            //    const passportConf = require('./middleware/client/auth') // changed passport plugin
            //    const isAuth =  passport.authenticate('auth', { session: false }) // check Auth
            //    const isFreelancer =  passport.authenticate('freelancer', { session: false }) // check Auth
            //    const isEmployer =  passport.authenticate('employer', { session: false }) // check Auth
            //    //////////////////////////////// CLient Router //////////////////////////////////
            //    this.#app.use('/client/resume', require('./resume/rest-api/auth'))
            //    // no Middleware, can access all ( Page )
            //    this.#app.use('/client/home', require('./routes/api/client/home')) // Home router
            //    //app.use('/client/search', require('./routes/api/client/search')) // Search router
            //    this.#app.use('/client/auth', require('./routes/api/client/auth')) // auth router ( SignIn, SignUp )
            //    this.#app.use('/client/evidence', require('./routes/api/client/evidence')) // evidence router
            //    this.#app.use('/client/search', require('./routes/api/client/search')) // seacrh router
            //    this.#app.use('/client/category', require('./routes/api/client/category')) // category router
            //    this.#app.use('/client/review', require('./routes/api/client/review')) // ( isAuth inside) review router
            //    // Middleware isAuth
            //    this.#app.use('/client/profile', isAuth, require('./routes/api/client/profile')) // profile router
            //    this.#app.use('/client/message', isAuth, require('./routes/api/client/message')) // message router
            //    this.#app.use('/client/order', isAuth, require('./routes/api/client/order')) // ( isFreelancer inside ), order router
            //    // Only freelancer access
            //    this.#app.use('/client/portfolio',
            // //  [ isAuth, isFreelancer ],
            //    require('./routes/api/client/portfolio')) // portfolio router
            //    //////////////////////////////// Admin Router /////////////////////////////////////////
            //    this.#app.use('/admin/portfolio', require('./routes/api/admin/portfolio')) // admin portfolio
            //    this.#app.use('/admin/category', require('./routes/api/admin/category')) // admin category
            //    this.#app.use('/admin/user', require('./routes/api/admin/user')) // admin user
            //    this.#app.use('/admin/order', require('./routes/api/admin/order')) // admin order
            //    this.#app.use('/admin/evidence', require('./routes/api/admin/evidence')) // admin evidence
        }
        initSocket() {
            this.#io = socket_io_1.default(this.#httpServer);
            this.#io.attach(3000, {
                pingInterval: 10000,
                pingTimeout: 5000,
                cookie: false
            });
            new socketio_1.default(this.#io);
        }
        listen() {
            this.#httpServer.listen(this.#port, () => {
                console.log('Http is runing at', `http://localhost`);
            });
            //  if AWS
            if (process.env.NODE_ENV === 'production') {
                this.#httpsServer.listen(443, () => {
                    console.log('Https is runing');
                });
            }
        }
    }
    App.PORT = 80;
    return App;
})();
new App();
